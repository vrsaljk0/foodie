#ifndef CASHIER_H
#define CASHIER_H

#include <QObject>

class Cashier : public QObject
{
    Q_OBJECT
public:
    explicit Cashier(QObject *parent = nullptr);

signals:

public slots:
    bool addCashier(const QString& name, const QString surname, const QString username, const QString role, const QString password, const QString age, const QString OIB);
};

#endif // CASHIER_H

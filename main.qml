import QtQuick 2.15
import QtQuick.Controls 2.15

import "./Constants.js" as Constants

import "./pages/components/buttons" as Buttons
import "./pages/components/modals" as Modals
import "./pages" as Pages

ApplicationWindow {
    id: window
    width: 1400
    height: 800
    visible: true
    title: qsTr(Constants.APP_TITLE)

    Pages.UserLoginPage {
        id: userLoginPage
    }

    header: ToolBar {
        id: headerToolBar
        visible: false
        contentHeight: toolButton.implicitHeight

        ToolButton {
            id: toolButton
            text: stackView.depth > 1 ? "\u25C0" : "\u2630"
            font.pixelSize: Qt.application.font.pixelSize * 1.6
            onClicked: {
                if (stackView.depth > 1) {
                    stackView.pop()
                } else {
                    drawer.open()
                }
            }
        }

        Label {
            text: stackView.currentItem.title
            anchors.centerIn: parent
        }
    }

    Drawer {
        id: drawer
        width: window.width * 0.2
        height: window.height

        Column {
            anchors.fill: parent
            ItemDelegate {
                id: addNewProductDelegate
                visible: false
                text: qsTr(Constants.APP_DRAWER_ADD_PRODUCT)
                width: parent.width
                onClicked: {
                    stackView.push("qrc:/pages/AddNewProductPage.qml")
                    drawer.close()
                }
            }

            ItemDelegate {
                id: deleteProductDelegate
                visible: false
                text: qsTr(Constants.APP_DRAWER_DELETE_PRODUCT)
                width: parent.width
                onClicked: {
                    stackView.push("qrc:/pages/DeleteProductPage.qml")
                    drawer.close()
                }
            }

            ItemDelegate {
                id: registerCashierDelegate
                visible: false
                text: qsTr(Constants.APP_DRAWER_REGISTER_CASHIER)
                width: parent.width
                onClicked: {
                    stackView.push("qrc:/pages/RegisterNewCashierPage.qml")
                    drawer.close()
                }
            }

            ItemDelegate {
                visible: true
                text: "Pregledaj današnje račune"
                width: parent.width
                onClicked: {
                    stackView.push("qrc:/pages/BillsListPage.qml")
                    drawer.close()
                }
            }

            ItemDelegate {
                text: qsTr(Constants.APP_DRAWER_LOGOUT)
                width: parent.width
                onClicked: {
                    headerToolBar.visible = false
                    stackView.visible = false
                    userLoginPage.visibility = true
                    drawer.close()
                }
            }
        }
    }

    StackView {
        id: stackView
        initialItem: "qrc:/pages/CashRegisterPage.qml"
        anchors.fill: parent
        visible: false
    }
}

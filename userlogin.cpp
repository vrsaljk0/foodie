#include "userlogin.h"

#include <QDebug>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlTableModel>

UserLogin::UserLogin(QObject *parent) : QObject(parent)
{

}

QString UserLogin::logUser(const QString& username, const QString& password)
{
    QSqlQuery querySelect;

    querySelect.prepare(QString("Select * from user where username = (:username) and password = (:password)"));
    querySelect.bindValue(":username", username);
    querySelect.bindValue(":password", password);

    if (!querySelect.exec()) {
        qDebug() << querySelect.lastError();
    }

    QString role = "";
    while (querySelect.next()) {
        role = querySelect.value(3).toString();
    }

    return role;
}

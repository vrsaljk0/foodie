#include "cashregister.h"

#include <ctime>
#include <iostream>

#include <QDebug>
#include <QSqlError>
#include <QSqlQuery>

using namespace std;

int totalPrice;

std::list<string> productsList;


CashRegister::CashRegister(QObject *parent) : QObject(parent)
{

}

int CashRegister::calculateItemPrice(int amount, int price)
{
    int itemPrice = amount * price;

    totalPrice += itemPrice;

    return itemPrice;
}

int CashRegister::getTotalPrice()
{
    return totalPrice;
}

void CashRegister::resetTotalPrice()
{
    totalPrice = 0;
}

int CashRegister::calculateCashChange(int givenMoney)
{
    int change = 0;

    if(givenMoney >= totalPrice) {
        change = givenMoney - totalPrice;
    } else {
        change = -1; // nedovoljno
    }

    return change;
}

void CashRegister::addProductToProductsList(QString productName) {
    productsList.push_back(productName.toStdString());

}

void CashRegister::addBill()
{
    string joinedProducts;
    for (auto &product: productsList) {
           joinedProducts.append(product + ",");
    }
    joinedProducts.pop_back();

    time_t rawtime;
    struct tm * timeinfo;
    char buffer[80];

    time (&rawtime);
    timeinfo = localtime(&rawtime);

    strftime(buffer,sizeof(buffer),"%d-%m-%Y %H:%M:%S",timeinfo);
    std::string str(buffer);

    QSqlQuery query;
    query.prepare("insert into bill (date, price, products) values(:date, :price, :products)");
    query.bindValue(":date", buffer);
    query.bindValue(":price",  QString::fromStdString(std::to_string(totalPrice) + "kn"));
    query.bindValue(":products", QString::fromStdString(joinedProducts));

    //reset values
    resetTotalPrice();
    productsList.clear();
    joinedProducts = "";

    if (!query.exec()) {
        qDebug() << query.lastError();
    }

}

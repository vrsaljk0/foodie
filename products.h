#ifndef PRODUCTS_H
#define PRODUCTS_H

#include <QObject>
#include <QSqlTableModel>

class Products : public QObject
{
    Q_OBJECT
public:
    explicit Products(QObject *parent = nullptr);

signals:

public slots:
    int addProduct(const QString& name, const QString price, const QString description, const QString category, const QString subcategory, const QString unit);
    bool removeProduct(const QString& id);
};

#endif // PRODUCTS_H

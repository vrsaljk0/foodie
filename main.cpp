#include <QDebug>
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QSqlError>
#include <QSqlTableModel>

#include <ctime>
#include <iostream>

#include "cashier.h"
#include "cashregister.h"
#include "dbmanager.h"
#include "products.h"
#include "productsmodel.h"
#include "userlogin.h"

std::string getCurrentDate() {
    time_t rawtime;
    struct tm * timeinfo;
    char buffer[80];

    time (&rawtime);
    timeinfo = localtime(&rawtime);

    strftime(buffer,sizeof(buffer),"%d-%m-%Y",timeinfo);
    std::string currentDate(buffer);

    return currentDate;
}

int main(int argc, char *argv[]) {
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;

    //handlers
    CashRegister cashRegisterHandler;
    DBManager dbHandler;
    Products productHandler;
    UserLogin userLoginHandler;
    Cashier cashierHandler;

    //models
    QSqlTableModel productsModel;
    productsModel.setTable("product");
    productsModel.select();

    QSqlTableModel billsModel;
    billsModel.setTable("bill");
    billsModel.setFilter("date like '%%" + QString::fromStdString(getCurrentDate()) + "%%'");
    billsModel.select();

    ProductsModel foodModel;
    foodModel.setTable("product");
    foodModel.setFilter("category= 'Hrana'");
    foodModel.select();

    ProductsModel drinkModel;
    drinkModel.setTable("product");
    drinkModel.setFilter("category= 'Piće'");
    drinkModel.select();

    engine.rootContext()->setContextProperty("product", &productHandler);
    engine.rootContext()->setContextProperty("productModel", &productsModel);
    engine.rootContext()->setContextProperty("foodModel", &foodModel);
    engine.rootContext()->setContextProperty("drinkModel", &drinkModel);
    engine.rootContext()->setContextProperty("billsModel", &billsModel);
    engine.rootContext()->setContextProperty("cashRegister", &cashRegisterHandler);
    engine.rootContext()->setContextProperty("userLogin", &userLoginHandler);
    engine.rootContext()->setContextProperty("cashier", &cashierHandler);

    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}



#include "productsmodel.h"

ProductsModel::ProductsModel(QObject *parent) : QSqlTableModel( parent )
{

}

QVariant ProductsModel::data(const QModelIndex &index, int role) const{

    QVariant value;
    if(role < Qt::UserRole)
    {
        value = QSqlTableModel::data(index, role);
    }
    else {
        int columnIdx = role - Qt::UserRole - 1;
        QModelIndex modelIndex = this->index(index.row(), columnIdx);
        value = QSqlTableModel::data(modelIndex, Qt::DisplayRole);
    }
    return value;
}

QHash<int, QByteArray> ProductsModel::roleNames() const
{
    QHash<int, QByteArray> roles;

    qDebug() << Qt::UserRole + 1;

    roles[IdRole] = "id";
    roles[NameRole] = "name";
    roles[PriceRole] = "price";
    roles[DescriptionRole] = "description";
    roles[CategoryRole] = "category";
    roles[SubcategoryRole] = "subcategory";
    roles[UnitRole] = "unit";

    return roles;
}

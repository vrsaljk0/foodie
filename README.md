# foodie

Cashier system for Foodie restaurant

Demo: https://youtu.be/LrqYdeeio2w

Requirements:

- Qt.5.15.2
- QtQuick 2.15
- QtQuick.Controls 2.15
- QtQuick.Layouts 1.15
- Qt.labs.qmlmodels 1.0
- QtQuick.Controls.Styles 1.4

First time setup:
- clone project
- import foodie.pro


Test user: 

username: admin

password: admin

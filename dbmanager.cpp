#include "dbmanager.h"

#include <QSqlDatabase>
#include <QDebug>
#include <QSqlError>
#include <QFile>

#include <QStandardPaths>
#include <QDir>
DBManager::DBManager(QObject *parent) : QObject(parent)
{
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    QFile file(":/database/foodie.db") ;
    QString foodieDbPath;

    if (/*file.exists()*/ file.open(QIODevice::ReadOnly) ) {
        // na linuxu, /home/user/.local/share dir
        foodieDbPath = QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation);

        if (foodieDbPath.isEmpty())
        {
            qDebug() << "Could not obtain writable location.";
            return;
        }

        QDir root = QDir::root();
        if(!root.mkpath(foodieDbPath))
        {
            qDebug() << "can't create path to " << foodieDbPath << ", bailing out";
            return;
        }

        foodieDbPath.append("/foodie.db");

        if(QFile::exists(foodieDbPath)) {
            QFile::remove(foodieDbPath);
        }

        if(QFile::copy(file.fileName(), foodieDbPath))
        {
            qDebug() << "created copy: " << foodieDbPath;
        } else {
            qDebug() << "copy alredy exists: " << foodieDbPath;
        }


        QFile::setPermissions(foodieDbPath ,QFile::WriteOwner | QFile::ReadOwner) ;
    } else qDebug() << "the file does not exist" ;
    db.setDatabaseName(foodieDbPath);

    if(!db.open()){
        qDebug() << db.lastError();
    }
}

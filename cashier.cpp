#include "cashier.h"

#include <QDebug>
#include <QSqlError>
#include <QSqlQuery>

Cashier::Cashier(QObject *parent) : QObject(parent)
{

}

bool Cashier::addCashier (const QString& name, const QString surname, const QString username, const QString role, const QString password, const QString age, const QString OIB)
{
    bool success = true;
    QSqlQuery queryAdd;

    queryAdd.prepare("insert into user (username, password, role, name, surname, age, oib) "
                    "values (:username, :password, :role, :name, :surname, :age, :oib)");
    queryAdd.bindValue(":username", username);
    queryAdd.bindValue(":password", password);
    queryAdd.bindValue(":username", username);
    queryAdd.bindValue(":role", role);
    queryAdd.bindValue(":name", name);
    queryAdd.bindValue(":surname", surname);
    queryAdd.bindValue(":age", age);
    queryAdd.bindValue(":oib", OIB);

    if (!queryAdd.exec()) {
        qDebug() << queryAdd.lastError();
        success = false;
    }

    return success;
}


#include "products.h"

#include <QDebug>
#include <QSqlError>
#include <QSqlQuery>

Products::Products(QObject *parent) : QObject(parent)
{

}

int Products::addProduct(const QString& name, const QString price, const QString description, const QString category, const QString subcategory, const QString unit)
{

    QSqlQuery querySelect;

    querySelect.prepare(QString("Select * from product where name = (:name) COLLATE NOCASE"));
    querySelect.bindValue(":name", name);

    if(!querySelect.exec())
    {
        qDebug() << querySelect.lastError();
    }

    int size = 0;
    while (querySelect.next()) {
        qDebug() << querySelect.value(1).toString();
        size++;
    }

    if(size > 0) {
        return -1;
    }

    int success = 1;
    QSqlQuery queryAdd;

    queryAdd.prepare("insert into product (name, price, description, category, subcategory, unit)"
                     " values (:name, :price, :description, :category, :subcategory, :unit)");
    queryAdd.bindValue(":name", name);
    queryAdd.bindValue(":price", price);
    queryAdd.bindValue(":description", description);
    queryAdd.bindValue(":category", category);
    queryAdd.bindValue(":subcategory", subcategory);
    queryAdd.bindValue(":unit", unit);

    if (!queryAdd.exec()) {
        qDebug() << queryAdd.lastError();
        success = 0;
    }

    return success;
}

bool Products::removeProduct(const QString& id)
{
    bool success = true;
    QSqlQuery queryRemove;

    queryRemove.prepare("delete from product where id = :id");
    queryRemove.bindValue(":id", id);

    if (!queryRemove.exec()) {
        qDebug() << queryRemove.lastError();
        success = false;
    }

    return success;
}



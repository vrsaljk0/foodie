#ifndef USERLOGIN_H
#define USERLOGIN_H

#include <QObject>

class UserLogin : public QObject
{
    Q_OBJECT
public:
    explicit UserLogin(QObject *parent = nullptr);

signals:

public slots:
   QString logUser(const QString& username, const QString& password);
};

#endif // USERLOGIN_H

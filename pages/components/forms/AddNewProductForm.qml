import QtQuick 2.15
import QtQuick.Controls 2.15

import "../../../Constants.js" as Constants

import "../buttons" as Buttons
import "../modals" as Modals

Frame {
    id: root
    width: parent.width
    height: parent.height
    anchors.centerIn: parent

    Modals.SuccessPopup {
        id: popup
        successMessage: Constants.SUCCESSFULY_ADDED
    }

    Item {
        id: item1
        x: 0
        y: 0
        width: root.width
        height: root.height

        Label {
            id: label
            x: 19
            y: 8
            text: qsTr("Dodaj novi proizvod")
            anchors.bottom: addNewProductGrid.top
            anchors.horizontalCenterOffset: -11
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottomMargin: 79
            font.bold: true
            font.pointSize: 16
        }

        Grid {
            id: addNewProductGrid
            height: 400
            columns: 2
            rowSpacing: 10
            columnSpacing: 10
            anchors.centerIn: parent
            anchors.verticalCenterOffset: 0
            anchors.horizontalCenterOffset: -11

            Label {
                id: productNameField
                text: Constants.PRODUCT_NAME
                font.pointSize: 12
            }
            TextField {
                id: productName
            }

            Label {
                id: productPriceLabel
                text: Constants.PRODUCT_PRICE
                font.pointSize: 12
            }
            TextField {
                id: productPrice
                font.pixelSize: 12
                validator: RegExpValidator {
                    regExp: /[0-9]+\.[0-9]+/
                }
            }

            Label {
                id: productDescriptionLabel
                text: Constants.PRODUCT_DESCRIPTION
                font.pointSize: 12
            }

            TextArea {
                id: productDescription
            }

            Label {
                id: productUnitLabel
                text: Constants.PRODUCT_UNIT
                font.pointSize: 12
            }

            ComboBox {
                id: productUnit
                model: [Constants.PRODUCT_UNIT_PORTION, Constants.PRODUCT_UNIT_LITER]
            }

            Label {
                id: productCategoryLabel
                text: Constants.PRODUCT_CATEGORY
                font.pointSize: 12
            }

            ComboBox {
                id: productCategory
                model: ["Hrana", "Piće"]
                onCurrentTextChanged: {
                    if (productCategory.currentText === Constants.PRODUCT_DRINKS) {
                        productDrinksCategory.visible = true
                        productFoodCategory.visible = false
                    } else {
                        productDrinksCategory.visible = false
                        productFoodCategory.visible = true
                    }
                }
            }

            Label {
                id: productFoodCategoryLabel
                text: Constants.PRODUCT_FOOD_SUBCATEGORY
                font.pointSize: 12
                visible: productFoodCategory.visible
            }

            ComboBox {
                id: productFoodCategory
                model: ListModel {
                    id: food
                    ListElement {
                        text: "Hladno predjelo"
                    }
                    ListElement {
                        text: "Toplo predjelo"
                    }
                    ListElement {
                        text: "Glavno jelo"
                    }
                    ListElement {
                        text: "Desert"
                    }
                    ListElement {
                        text: "Ostalo"
                    }
                }
                visible: false
            }

            Label {
                id: productDrinksCategoryLabel
                text: Constants.PRODUCT_FOOD_SUBCATEGORY
                font.pointSize: 12
                visible: productDrinksCategory.visible
            }
            ComboBox {
                id: productDrinksCategory
                model: ListModel {
                    id: drinks
                    ListElement {
                        text: "Alkoholno"
                    }
                    ListElement {
                        text: "Bezalkoholno"
                    }
                    ListElement {
                        text: "Topli napitci"
                    }
                }
                visible: false
            }
        }

        Buttons.PrimaryButton {
            id: submitButton
            y: 475
            width: 180
            buttonText: Constants.PRODUCT_ADD_NEW_PRODUCT
            height: 80
            anchors.bottom: addNewProductGrid.top
            buttonWidth: 300
            font.pointSize: 14
            anchors.horizontalCenterOffset: -11
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottomMargin: -435
            onClicked: {
                //Call a slot
                var subcategory = productFoodCategory.currentText
                console.log(productCategory.currentText)
                if (productCategory.currentText === "Piće") {
                    subcategory = productDrinksCategory.currentText
                }

                var addNewProduct = product.addProduct(
                            productName.text, productPrice.text,
                            productDescription.text,
                            productCategory.currentText, subcategory,
                            productUnit.currentText)
                console.log("QML:", addNewProduct);
                if (addNewProduct === 1) {
                    popup.successMessage = Constants.SUCCESSFULY_ADDED
                    popup.open()
                    productName.clear()
                    productPrice.clear()
                    productDescription.clear()
                    productModel.select()
                    foodModel.select()
                    drinkModel.select()
                } else if (addNewProduct === -1) {
                    popup.successMessage = "Takav proizvod već postoji!"
                    popup.open()
                    productName.clear()
                    productPrice.clear()
                    productDescription.clear()
                    productModel.select()
                    foodModel.select()
                    drinkModel.select()
                } else {
                    popup.successMessage = "Došlo je do pogreške!"
                    popup.open()
                    productName.clear()
                    productPrice.clear()
                    productDescription.clear()
                    productModel.select()
                    foodModel.select()
                    drinkModel.select()
                }
            }
        }

    }
}

import QtQuick 2.15
import QtQuick.Controls 2.15

import "../../../Constants.js" as Constants

import "../buttons" as Buttons
import "../modals" as Modals

Frame {
    id: root
    width: parent.width
    height: parent.height
    anchors.centerIn: parent

    Modals.SuccessPopup {
        id: addCashierPopup
        popupHeight: 160
        successMessage: "Uspjesno registriran!"
    }

    Item {
        id: item1
        width: root.width
        height: root.height

        Label {
            id: label
            x: -12
            text: qsTr("Registriraj novog blagajnika")
            anchors.top: addNewProductGrid.top
            anchors.horizontalCenterOffset: 1
            anchors.horizontalCenter: addNewProductGrid.horizontalCenter
            font.bold: true
            font.pointSize: 16
            anchors.topMargin: -104
        }

        Grid {
            id: addNewProductGrid
            height: 400
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            columns: 2
            rowSpacing: 10
            columnSpacing: 10

            Label {
                text: Constants.CASHIER_NAME
                font.pointSize: 12
            }

            TextField {
                id: cashierName
            }

            Label {
                text: Constants.CASHIER_SURNAME
                font.pointSize: 12
            }

            TextField {
                id: cashierSurname
            }

            Label {
                text: Constants.CASHIER_USERNAME
                font.pointSize: 12
            }

            TextField {
                id: cashierUsername
            }
            Label {
                text: Constants.CASHIER_AGE
                font.pointSize: 12
            }

            TextField {
                id: cashierAge
                validator: RegExpValidator {
                    regExp: /[0-9]+\.[0-9]+/
                }
            }

            Label {
                id: cashierRoleLabel
                text: Constants.CASHIER_ROLE
                font.pointSize: 12
            }

            ComboBox {
                id: cashierRole
                model: ListModel {
                    id: roles
                    ListElement {
                        text: "cashier"
                    }
                    ListElement {
                        text: "admin"
                    }
                }

            }

            Label {
                text: Constants.CASHIER_OIB
                font.pointSize: 12
            }

            TextField {
                id: cashierOIB
                validator: RegExpValidator {
                    regExp: /^(?:HR)?(\d{10}(\d))$/
                }
            }

            Label {
                text: Constants.CASHIER_PASSWORD
                font.pointSize: 12
            }

            TextField {
                id: cashierPassword

            }
        }

        Buttons.PrimaryButton {
            id: submitButton
            x: 75
            anchors.top: addNewProductGrid.bottom
            anchors.horizontalCenterOffset: 0
            font.pointSize: 12
            anchors.horizontalCenter: addNewProductGrid.horizontalCenter
            anchors.topMargin: 46
            buttonText: Constants.CASHIER_REGISTER
            buttonHeight: 80
            buttonWidth: 300

            onClicked: {
                var registerCashier = cashier.addCashier(cashierName.text, cashierSurname.text, cashierUsername.text, cashierRole.currentText, cashierPassword.text,
                                                         cashierAge.text, cashierOIB.text)
                if (registerCashier) {
                    addCashierPopup.open()
                    cashierName.clear()
                    cashierSurname.clear()
                    cashierUsername.clear()
                    cashierPassword.clear()
                    cashierAge.clear()
                    cashierOIB.clear()
                }
            }
        }

    }

}

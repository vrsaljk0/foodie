import QtQuick 2.15
import QtQuick.Controls 2.15

import "../../../Constants.js" as Constants

import "../buttons" as Buttons

Popup {
    id: popup
    anchors.centerIn: parent
    width: 400
    height: 200
    modal: true
    focus: true
    closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutside

    property alias successMessage: succesLabel.text
    property alias popupHeight: popup.height

    Column {
        id: popupColumn
        anchors.fill: parent
        anchors.verticalCenter: parent.verticalCenter
        spacing: 40

        Label {
            id: succesLabel
            width: 400
            height: 60
            anchors.verticalCenter: parent.verticalCenter
            horizontalAlignment: Text.AlignHCenter
            anchors.verticalCenterOffset: -20
            anchors.horizontalCenterOffset: 0
            anchors.horizontalCenter: parent.horizontalCenter
            font.bold: true
        }

        Buttons.SecondaryButton {
            id: closeButton
            buttonText: Constants.CLOSE
            font.pixelSize: 20
            anchors.bottom: parent.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottomMargin: 12
            onClicked: popup.close()
        }
    }
}

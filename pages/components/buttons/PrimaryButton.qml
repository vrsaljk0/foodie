import QtQuick 2.0
import QtQuick.Controls 2.15

Button {
    property alias buttonWidth: primaryButton.width
    property alias buttonHeight: primaryButton.height
    property alias buttonText: primaryButton.text

    id: primaryButton
    font.bold: true

    background: Rectangle {
        opacity: enabled ? 1 : 0.3
        color: "#0000CD"
        radius: 10
    }
    contentItem: Text {
        text: primaryButton.text
        font: primaryButton.font
        color: "white"
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
    }
}

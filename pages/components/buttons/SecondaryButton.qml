import QtQuick 2.0
import QtQuick.Controls 2.15

Button {
    property alias buttonWidth: secondaryButton.width
    property alias buttonHeight: secondaryButton.height
    property alias buttonText: secondaryButton.text

    id: secondaryButton
    font.bold: true

    background: Rectangle {
        opacity: enabled ? 1 : 0.3
        color: "#1E90FF"
        radius: 10
    }
    contentItem: Text {
        text: secondaryButton.text
        font: secondaryButton.font
        color: "white"
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
    }
}

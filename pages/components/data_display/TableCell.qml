import QtQuick 2.0
import QtQuick.Layouts 1.15
import Qt.labs.qmlmodels 1.0
import QtQuick.Controls.Styles 1.4

Rectangle {
    id: cell

    property alias columnText: columnText.text
    property alias cellTextWidth: columnText.width
    property alias cellWidth: cell.implicitWidth

    implicitWidth: 120
    implicitHeight: 60
    border.width: 0.5
    color: "#87CEFA"

    Text {
        id: columnText
        anchors.centerIn: parent
        anchors.leftMargin: 10
        wrapMode: Text.WordWrap // ne radi...
    }
}

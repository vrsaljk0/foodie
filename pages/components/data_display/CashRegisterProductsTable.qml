import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import Qt.labs.qmlmodels 1.0
import QtQuick.Controls.Styles 1.4

import "../../../Constants.js" as Constants

import "../buttons" as Buttons

ColumnLayout {
    id: billProductsList

    property alias isVisible: billProductsList.visible

    property var selectedProductName: ""
    property var selectedProductPrice: ""
    property int totalPrice: 0
    property alias productsModel: allProductsTable.model

    Layout.alignment: Qt.AlignHCenter | Qt.AlignTop

    RowLayout {
        id: columnHeaderNames
        Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
        spacing: 0

        Rectangle {
            width: allProductsTable.width / 5
            height: 20

            Text {
                id: nameLabel
                text: qsTr(Constants.PRODUCT_NAME)
                anchors.centerIn: parent
                font.bold: true
            }
        }

        Rectangle {
            width: allProductsTable.width / 5
            height: 20
            Text {
                id: productLabel
                text: qsTr(Constants.PRODUCT_PRICE)
                anchors.centerIn: parent
                font.bold: true
            }
        }

        Rectangle {
            width: allProductsTable.width / 5
            height: 20
            Text {
                id: subcategoryLabel
                text: Constants.PRODUCT_CATEGORY
                anchors.centerIn: parent
                font.bold: true
            }
        }

        Rectangle {
            width: allProductsTable.width / 5
            height: 20
            Text {
                id: unitLabel
                text: Constants.PRODUCT_UNIT
                anchors.centerIn: parent
                font.bold: true
            }
        }

        Rectangle {
            width: allProductsTable.width / 5
            height: 20
            Text {
                id: actionLabel
                text: Constants.ADD_TO_BILL
                anchors.centerIn: parent
                font.bold: true
            }
        }
    }

    TableView {
        id: allProductsTable
        Layout.alignment: Qt.AlignHCenter
        width: 600
        height: 400
        interactive: false

        ScrollBar.vertical: ScrollBar {
            id: tableVerticalBar
            policy: ScrollBar.AsNeeded
        }

        delegate: DelegateChooser {
            DelegateChoice {
                column: 0
                delegate: TableCell {
                    columnText: name
                }
            }
            DelegateChoice {
                column: 1
                delegate: TableCell {
                    columnText: price
                }
            }
            DelegateChoice {
                column: 2
                delegate: TableCell {
                    columnText: subcategory
                }
            }
            DelegateChoice {
                column: 3
                delegate: TableCell {
                    columnText: unit
                }
            }
            DelegateChoice {
                column: 4
                delegate: TableCell {
                    Buttons.PrimaryButton {
                        buttonWidth: 60
                        buttonHeight: 40
                        buttonText: Constants.ADD
                        id: addButton
                        signal sendSelectedValuesToPopup(string name, string price)
                        anchors.centerIn: parent
                        onClicked: {
                            addButton.sendSelectedValuesToPopup(name, price)
                        }
                        onSendSelectedValuesToPopup: productAmountDialog.receiveSelectedValueFromDataTable(
                                                         name, price)
                    }
                }
            }
        }
    }

    Dialog {
        id: productAmountDialog
        anchors.centerIn: parent
        title: Constants.PRODUCT_QUANTITY
        standardButtons: Dialog.Ok | Dialog.Cancel
        signal receiveSelectedValueFromDataTable(string name, string price)
        onReceiveSelectedValueFromDataTable: {
            productAmountDialog.open()
            selectedProductName = name
            selectedProductPrice = price
        }

        onAccepted: {
            var productPrice = cashRegister.calculateItemPrice(
                        selectedProductPrice, productAmount.text)
            totalPriceText.text = cashRegister.getTotalPrice() + " kn"
            payBillButton.enabled = true
            billProductsModel.append({
                                         "name": selectedProductName,
                                         "price": selectedProductPrice,
                                         "amount": productAmount.text,
                                         "total": productPrice
                                     })
            cashRegister.addProductToProductsList(selectedProductName);
            productAmount.text = "1"
            productAmountDialog.close()
        }

        onRejected: {
            selectedProductName = ""
            selectedProductPrice = ""
            productAmountDialog.close()
        }

        Grid {
            id: productAmountGrid
            columns: 2
            rowSpacing: 10
            columnSpacing: 10
            anchors.centerIn: parent
            height: parent.height

            Label {
                text: Constants.AMOUNT
            }

            TextField {
                id: productAmount
                validator: RegExpValidator {
                    regExp: /[0-9]+\.[0-9]+/
                }
                text: "1"
            }
        }
    }
}

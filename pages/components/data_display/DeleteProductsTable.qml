import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import Qt.labs.qmlmodels 1.0

import "../../../Constants.js" as Constants

import "../buttons" as Buttons

Frame {
    id: deleteProductsFrame
    width: parent.width
    height: parent.height

    TableView {
        id: deleteProductsTable
        width: 600
        height: 400
        anchors.top: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        interactive: false
        boundsMovement: Flickable.StopAtBounds
        boundsBehavior: Flickable.StopAtBounds
        anchors.topMargin: -120
        model: productModel
        Layout.alignment: Qt.AlignHCenter | Qt.AlignTop

        ScrollBar.vertical: ScrollBar {
            id: tableVerticalBar
            policy: ScrollBar.AsNeeded
        }

        delegate: DelegateChooser {
            DelegateChoice {
                column: 0
                delegate: TableCell {
                    Buttons.PrimaryButton {
                        id: deleteButton
                        buttonText: Constants.DELETE
                        anchors.centerIn: parent
                        signal sendSelectedValueToDeleteDialog(string value)
                        onClicked: {
                            deleteButton.sendSelectedValueToDeleteDialog(model.display)
                        }
                        onSendSelectedValueToDeleteDialog: deleteDialog.receiveSelectedValueFromDataTable(
                                                        value)
                    }
                }

            }

            DelegateChoice {
                delegate: TableCell {
                    columnText: display
                }
            }
        }

        Dialog {
            id: deleteDialog
            anchors.centerIn: parent
            width: 400
            height: 160
            title: Constants.DELETE_MESSAGE
            standardButtons: Dialog.Ok | Dialog.Cancel

            property var selectedProduct: ""

            signal receiveSelectedValueFromDataTable(string value)
            onReceiveSelectedValueFromDataTable: {
                deleteDialog.open()
                selectedProduct = value
            }

            onAccepted: {
                text: Constants.YES
                product.removeProduct(deleteDialog.selectedProduct)
                productModel.select()
                foodModel.select()
                drinkModel.select()
                deleteProductsTable.model = productModel
                deleteDialog.close()
            }

            onRejected: {
                text: Constants.CANCEL
                deleteDialog.close()
            }
        }
    }

    Label {
        id: label
        x: -20
        width: 391
        height: 0
        text: Constants.DELETE_PRODUCTS
        anchors.top: columnHeaderNames.bottom
        anchors.bottom: deleteProductsTable.top
        anchors.bottomMargin: 100
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.topMargin: -180
        font.bold: true
        font.pointSize: 16
    }
}

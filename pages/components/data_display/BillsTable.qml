import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import Qt.labs.qmlmodels 1.0

import "../../../Constants.js" as Constants

import "../buttons" as Buttons

Frame {
    id: billsFrame
    width: parent.width
    height: parent.height

    TableView {
        id: billsTable
        width: 1000
        height: 400
        anchors.top: parent.verticalCenter
        anchors.horizontalCenterOffset: 0
        bottomMargin: 40
        synchronousDrag: true
        topMargin: 40
        anchors.horizontalCenter: parent.horizontalCenter
        interactive: false
        boundsMovement: Flickable.StopAtBounds
        boundsBehavior: Flickable.StopAtBounds
        anchors.topMargin: -194
        model: billsModel
        Layout.alignment: Qt.AlignHCenter | Qt.AlignTop

        ScrollBar.vertical: ScrollBar {
            id: tableVerticalBar
            policy: ScrollBar.AsNeeded
        }

        delegate: DelegateChooser {
            DelegateChoice {
                delegate: TableCell {
                    id: billcell
                    cellWidth: 250
                    cellTextWidth: 200

                    //columnText: "aaaaaaa aaaaaa aaaaa aaaaaaaaaa aaaaaaaaaaaaaaa aaaaaaa" //word wrap radi
                    columnText: display //word wrap ne radi
                }
            }
        }
    }

    Label {
        id: label
        x: 77
        y: 61
        text: qsTr("Današnji računi")
        anchors.horizontalCenterOffset: 0
        font.bold: true
        anchors.horizontalCenter: billsTable.horizontalCenter
        font.pointSize: 16
    }
}

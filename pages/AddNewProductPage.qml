import QtQuick 2.15
import QtQuick.Controls 2.15

import "./components/forms" as Forms

Page {
    id: page
    width: parent.width
    height: parent.height
    title: qsTr("Foodie restaurant - Admin")

    Forms.AddNewProductForm {}
}

import QtQuick 2.15
import QtQuick.Controls 2.15
import Qt.labs.qmlmodels 1.0

import "../Constants.js" as Constants

import "./components/data_display" as DataDisplay

Page {
    id: root
    width: parent.width
    height: parent.height
    title: qsTr(Constants.APP_TITLE)

    DataDisplay.DeleteProductsTable {}
}

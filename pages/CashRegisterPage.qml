import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import Qt.labs.qmlmodels 1.0
import QtQuick.Controls.Styles 1.4

import "../Constants.js" as Constants

import "./components/buttons" as Buttons
import "./components/data_display" as DataDisplay
import "./components/modals" as Modals

Page {
    id: root
    width: parent.width
    height: parent.height
    title: qsTr(Constants.APP_TITLE)

    function setTotalPriceText() {
        totalPriceText.text = cashRegister.getTotalPrice() + " kn";
    }

    function resetBill() {
        billProductsModel.clear()
        billProductsList.model = billProductsModel
        payBillButton.enabled = false
        cashRegister.resetTotalPrice()
        totalPriceText.text = setTotalPriceText();
    }

    RowLayout {
        id: layout
        anchors.fill: parent
        anchors.rightMargin: 0
        anchors.bottomMargin: 0
        anchors.leftMargin: 0
        anchors.topMargin: 0

        Frame {
            id: productsListFrame
            Layout.fillWidth: false
            Layout.preferredWidth: parent.width / 2
            Layout.fillHeight: true

            SwipeView
            {
                id: swipeView
                anchors.fill: parent
                width: parent.width
                clip: true
                Page
                {
                    id: page
                    title: "Food page"
                    DataDisplay.CashRegisterProductsTable {
                        isVisible: true
                        productsModel: foodModel
                        anchors.centerIn: parent
                    }

                }
                Page
                {
                    id: page1
                    title: "Drinks model"
                    DataDisplay.CashRegisterProductsTable {
                        isVisible: true
                        productsModel: drinkModel
                        anchors.centerIn: parent
                    }
                }
            }


            PageIndicator
            {
                id: pageIndicator
                anchors.bottom: swipeView.bottom
                anchors.bottomMargin: 10
                anchors.horizontalCenter: swipeView.horizontalCenter
                count: swipeView.count
                currentIndex: swipeView.currentIndex
            }




            TabBar {
                id: tabBar
                width: productsListFrame.width - 24
                height: 60
                layer.effect: root
                clip: true
                font.weight: Font.ExtraLight
                enabled: true
                currentIndex: swipeView.currentIndex


                TabButton {
                    id: tabButton
                    height: 40
                    background: Rectangle {
                        color: tabBar.currentIndex == 0 ? "#87CEFA" : "#9C9C9C"
                        radius: 2
                        Label {
                            text: qsTr("Hrana")
                            font.bold: true
                            font.pointSize: 14
                            anchors.centerIn: parent
                            color: "white"
                        }
                    }

                    onClicked: {
                        swipeView.currentIndex = 0
                    }
                }

                TabButton {
                    id: tabButton1
                    height: 40
                    background: Rectangle {
                        color: tabBar.currentIndex == 1 ? "#87CEFA" : "#9C9C9C"
                        radius: 2
                        Label {
                            text: qsTr("Piće")
                            font.bold: true
                            font.pointSize: 14
                            anchors.centerIn: parent
                            color: "white"
                        }
                    }

                    onClicked: {
                        swipeView.currentIndex = 1
                    }
                }
            }
        }

        Frame {
            id: bill
            Layout.fillWidth: true
            Layout.fillHeight: true

            ColumnLayout {
                width: bill.width
                height: bill.height
                spacing: 10

                Rectangle {
                    Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
                    Label {
                        text: Constants.CASH_REGISTER_CURRENT_BILL
                        font.bold: true
                        font.pixelSize: 20
                    }
                }

                RowLayout {
                    width: parent.width
                    height: 60
                    spacing: 0
                    Layout.topMargin: 30
                    Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

                    Rectangle {
                        Layout.preferredWidth: parent.width / 4
                        Layout.preferredHeight: 60

                        Label {
                            text: Constants.PRODUCT_NAME
                            anchors.centerIn: parent
                            font.bold: true
                        }
                    }

                    Rectangle {
                        Layout.fillWidth: true
                        Layout.preferredHeight: 60

                        Label {
                            text: Constants.PRODUCT_PRICE
                            anchors.centerIn: parent
                            font.bold: true
                        }
                    }

                    Rectangle {
                        Layout.fillWidth: true
                        Layout.preferredHeight: 60

                        Label {
                            text: Constants.AMOUNT
                            anchors.centerIn: parent
                            font.bold: true
                        }
                    }

                    Rectangle {
                        Layout.fillWidth: true
                        Layout.preferredHeight: 60

                        Label {
                            text: Constants.TOTAL
                            anchors.centerIn: parent
                            font.bold: true
                        }
                    }
                }

                ListView {
                    id: billProductsList
                    Layout.bottomMargin: 30
                    Layout.leftMargin: 0
                    Layout.topMargin: 0
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                    model: ListModel {
                        id: billProductsModel
                    }

                    delegate: RowLayout {
                        width: parent.width
                        height: 60
                        anchors.verticalCenter: parent.verticalCente
                        anchors.horizontalCenterOffset: 0
                        anchors.horizontalCenter: parent.horizontalCenter
                        spacing: 0

                        DataDisplay.TableCell {
                            columnText: name
                            Layout.preferredWidth: parent.width / 4
                            Layout.preferredHeight: 60
                        }

                        DataDisplay.TableCell {
                            columnText: price
                            Layout.preferredWidth: parent.width / 4
                            Layout.preferredHeight: 60
                        }

                        DataDisplay.TableCell {
                            columnText: amount
                            Layout.preferredWidth: parent.width / 4
                            Layout.preferredHeight: 60
                        }

                        DataDisplay.TableCell {
                            columnText: total
                            Layout.preferredWidth: parent.width / 4
                            Layout.preferredHeight: 60
                        }
                    }
                }

                ColumnLayout {
                    Layout.preferredWidth: bill.width / 2
                    spacing: 20

                    RowLayout {
                        width: parent.width
                        height: 60
                        Layout.fillHeight: true
                        Layout.fillWidth: false
                        Layout.bottomMargin: 0
                        Layout.topMargin: 70
                        spacing: 0

                        Rectangle {
                            id: rectangle
                            color: "white"
                            Layout.preferredWidth: bill.width / 2
                            Layout.preferredHeight: 60

                            Label {
                                text: qsTr(Constants.TOTAL)
                                anchors.verticalCenter: parent.verticalCenter
                                font.bold: true
                                font.pixelSize: 18
                            }
                        }

                        Rectangle {
                            Layout.preferredWidth: bill.width / 2
                            Layout.preferredHeight: 60
                            Text {
                                id: totalPriceText
                                text: setTotalPriceText()
                                font.pixelSize: 18
                                font.bold: true
                                anchors.centerIn: parent
                            }
                        }
                    }

                    RowLayout {
                        id: buttonLayout
                        Layout.bottomMargin: 30
                        Layout.topMargin: 0
                        Layout.minimumWidth: 2
                        Layout.alignment: Qt.AlignLeft | Qt.AlignBottom
                        Layout.fillHeight: true
                        Layout.fillWidth: true
                        Layout.preferredWidth: bill.width / 2

                        Buttons.SecondaryButton {
                            id: cancelBillButton
                            Layout.preferredWidth: parent.width / 2

                            Layout.preferredHeight: 60
                            buttonText: Constants.CANCEL_BILL
                            onClicked: {
                                resetBill()
                            }
                        }
                        Buttons.PrimaryButton {
                            id: payBillButton
                            Layout.fillWidth: true
                            Layout.preferredHeight: 60
                            text: Constants.PAY_BILL
                            enabled: false
                            onClicked: {
                                acceptedMoney.text = ""
                                notEnoughMoney.visible = false
                                cashChangeDialog.open()
                            }
                        }
                    }
                }
            }
        }

    }

    Modals.SuccessPopup {
        id: successPopup
        successMessage: Constants.PAY_BILL_SUCCESS
    }

    Dialog {
        id: cashChangeDialog
        anchors.centerIn: parent
        title: "Zaprimljeno novaca:"
        width: 300
        height: 250

        signal receiveSelectedValueFromDataTable(string name, string price)
        onReceiveSelectedValueFromDataTable: {
            cashChangeDialog.open()
            selectedProductName = name
            selectedProductPrice = price
        }

        ColumnLayout {
            anchors.centerIn: parent
            height: parent.height
            spacing: 30

            TextField {
                id: acceptedMoney
                validator: RegExpValidator {
                    regExp: /[0-9]+\.[0-9]+/
                }
                width: 100
                text: ""
                focus: true
                onTextChanged: {
                    var change = cashRegister.calculateCashChange(acceptedMoney.text)
                    if(change < 0) {
                        notEnoughMoney.visible = true
                        changeMoney.visible = false
                        payButton.enabled = false
                    } else {
                        changeMoney.visible = true
                        changeMoney.text = "Vratite:  " + change + " kn"
                        notEnoughMoney.visible = false
                        payButton.enabled = true
                    }
                }
            }

            Text {
                id: changeMoney
                visible: false
                font.pointSize: 14
                font.bold: true
            }
            Text {
                id: notEnoughMoney
                visible: true
                color: "red"
                font.bold: true
                font.pointSize: 14
                text: "Nedovoljno!"
            }

            RowLayout {

                Buttons.SecondaryButton {
                    id: cancelButton
                    text: "Odustani"
                    Layout.topMargin: 10
                    onClicked: {
                        cashChangeDialog.close()
                        changeMoney.text = ""
                        acceptedMoney.text = ""
                        changeMoney.visible = false
                        notEnoughMoney.visible = false
                    }
                }

                Buttons.PrimaryButton {
                    function addNewBill() {
                        cashRegister.addBill()
                        totalPriceText.text = cashRegister.getTotalPrice() + " kn"
                        billProductsModel.clear()
                        billsModel.select()
                        cashChangeDialog.close()
                        payBillButton.enabled = false
                    }

                    id: payButton
                    text: "Naplati"
                    Layout.topMargin: 10
                    enabled: false
                    focus: true
                    onClicked: addNewBill()
                }

                Component.onCompleted: payButton.forceActiveFocus(Qt.PopupFocusReason)
            }
        }
    }
}

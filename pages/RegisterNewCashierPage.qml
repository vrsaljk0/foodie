import QtQuick 2.0
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

import "../Constants.js" as Constants

import "./components/forms" as Forms

Page {
    id: page
    width: parent.width
    height: parent.height
    title: qsTr(Constants.APP_TITLE)

    Forms.RegisterNewCashierForm {}
}


import QtQuick 2.15
import QtQuick.Controls 2.15

import "../Constants.js" as Constants

import "./components/buttons" as Buttons
import "./components/data_display" as DataDisplay
import "./components/modals" as Modals

Page {
    id: userLoginPage
    anchors.centerIn: parent
    title: qsTr(Constants.APP_TITLE)

    property alias visibility: userLoginPage.visible
    Component.onCompleted: submitButton.forceActiveFocus(Qt.OtherFocusReason)

    function loginUser() {
        var role = userLogin.logUser(username.text, password.text)

        if (role === Constants.USER_ADMIN) {
            headerToolBar.visible = true
            addNewProductDelegate.visible = true
            deleteProductDelegate.visible = true
            registerCashierDelegate.visible = true
            stackView.visible = true
            userLoginPage.visibility = false
        } else if (role === Constants.USER_CASHIER) {
            stackView.visible = true
            headerToolBar.visible = true
            addNewProductDelegate.visible = false
            deleteProductDelegate.visible = false
            registerCashierDelegate.visible = false
            userLoginPage.visibility = false
        } else {
            loginPopup.open()
        }
        username.clear()
        password.clear()
    }

    Label {
        id: label
        x: 244
        y: 12
        text: qsTr("Foodie restoran blagajna")
        color: "#0000CD"
        anchors.bottom: loginGrid.top
        anchors.horizontalCenterOffset: 1
        anchors.bottomMargin: 138
        anchors.horizontalCenter: loginGrid.horizontalCenter
        font.bold: true
        font.pointSize: 24
    }

    Label {
        id: label1
        x: 119
        y: 118
        text: qsTr("Prijavi se kao blagajnik ili administrator")
        anchors.bottom: loginGrid.top
        anchors.horizontalCenter: loginGrid.horizontalCenter
        anchors.bottomMargin: 40
        font.pointSize: 12
    }

    Buttons.PrimaryButton {
        id: submitButton
        buttonWidth: 160
        buttonHeight: 60
        anchors.bottom: loginGrid.bottom
        font.pointSize: 14
        anchors.bottomMargin: -83
        anchors.horizontalCenter: loginGrid.horizontalCenter
        buttonText: Constants.USER_LOGIN

        focus: true
        onClicked: loginUser()
        Keys.onReturnPressed: loginUser()
        Keys.onEnterPressed: loginUser()
    }

    Grid {
        id: loginGrid
        columns: 2
        rowSpacing: 10
        columnSpacing: 10

        anchors.centerIn: parent
        height: login.height / 2

        Label {
            id: usernameLabel
            text: Constants.USER_USERNAME
            font.pointSize: 12
            font.bold: true
        }

        TextField {
            id: username
            font.pointSize: 14
            Keys.onReturnPressed: loginUser()
            Keys.onEnterPressed: loginUser()
        }

        Label {
            id: passwordLabel
            text: Constants.USER_PASSWORD
            font.pointSize: 12
            font.bold: true
        }

        TextField {
            id: password
            font.pointSize: 14
            echoMode: TextInput.Password
            Keys.onReturnPressed: loginUser()
            Keys.onEnterPressed: loginUser()
        }

    }


    Modals.SuccessPopup {
        id: loginPopup
        popupHeight: 180
        successMessage: Constants.USER_LOGIN_MESSAGE
    }

}

/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}D{i:2}
}
##^##*/

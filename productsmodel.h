#ifndef PRODUCTSMODEL_H
#define PRODUCTSMODEL_H

#include <QObject>
#include <QDebug>
#include <QSqlTableModel>

class ProductsModel : public QSqlTableModel
{
    Q_OBJECT
public:
    enum ProductsRoles {
         IdRole = Qt::UserRole + 1,
         NameRole = Qt::UserRole + 2,
         PriceRole = Qt::UserRole + 3,
         DescriptionRole = Qt::UserRole + 4,
         CategoryRole = Qt::UserRole + 5,
         SubcategoryRole = Qt::UserRole + 6,
         UnitRole = Qt::UserRole + 7
     };

    ProductsModel(QObject* parent = nullptr);
    Q_INVOKABLE QVariant data(const QModelIndex &index, int role) const override;
    QHash<int, QByteArray> roleNames() const override;
};

#endif // PRODUCTSMODEL_H

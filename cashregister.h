#ifndef CASHREGISTER_H
#define CASHREGISTER_H

#include <QObject>

class CashRegister : public QObject
{
    Q_OBJECT
public:
    CashRegister(QObject *parent = nullptr);

signals:

public slots:
    int calculateItemPrice(int price, int amount);
    int getTotalPrice();
    void resetTotalPrice();
    int calculateCashChange(int givenMoney);
    void addProductToProductsList(QString productName);
    void addBill();
};

#endif // CASHREGISTER_H
